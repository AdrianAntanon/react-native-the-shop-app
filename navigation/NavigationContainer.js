import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import ShopNavigator from './ShopNavigator';

const NavigationContainer = (props) => {
  const navRef = useRef();

  // El !! fuerza a devolver un valor booleano, es decir, si state existe pues nos devolverá un true.
  const isAuth = useSelector(state => !!state.auth.token);

  useEffect(() => {
    if (!isAuth) {
      navRef.current.dispatch(
        NavigationActions.navigate({
          routeName: 'Auth'
        })
      );
    }
  }, [isAuth])

  return (
    <ShopNavigator ref={navRef} />
  );
}

export default NavigationContainer;