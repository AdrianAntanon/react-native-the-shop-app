import React, { useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

import CartItem from './CartItem';
import Card from '../UI/Card';
import Colors from '../../constants/Colors';

const OrderItem = (props) => {
    const [showDetails, setShowDetails] = useState(false);

    const { amount, date, items } = props;

    return (
        <Card style={styles.orderItem} >
            <View style={styles.summary}>
                <Text style={styles.totalAmount}>{amount.toFixed(2)}€</Text>
                <Text style={styles.date}>{date}</Text>
            </View>
            <Button
                color={Colors.primary}
                title={showDetails ? 'Hide Details' : "Show Details"}
                onPress={() => {
                    setShowDetails(prevState => !prevState)
                }}
            />
            {showDetails &&
                <View style={styles.detailItem}>
                    {items.map(cartitem =>
                        <CartItem
                            quantity={cartitem.quantity}
                            title={cartitem.productTitle}
                            amount={cartitem.sum}
                        />)}
                </View>
            }
        </Card>
    );
}

const styles = StyleSheet.create({
    orderItem: {
        backgroundColor: 'white',
        margin: 20,
        padding: 10,
        alignItems: 'center'
    },
    summary: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        marginBottom: 15,
    },
    totalAmount: {
        fontFamily: 'open-sans-bold',
        fontSize: 16
    },
    date: {
        fontFamily: 'open-sans',
        fontSize: 16,
        color: '#888',
    },
    detailItem: {
        width: '100%'
    }

});

export default OrderItem;