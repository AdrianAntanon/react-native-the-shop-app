import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = (props) => {
    return (
        <View style={{ ...styles.card, ...props.style }} >
            {props.children}
        </View>
    );
}

const styles = StyleSheet.create({
    card: {
        // Sombras en IOS
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        // Sombras en Android
        elevation: 5,
        // Fin sombras
        borderRadius: 10,
    }
});

export default Card;